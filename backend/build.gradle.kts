buildscript {
    repositories {
        mavenLocal()
        jcenter()
        mavenCentral()
    }
    dependencies {
        classpath("net.serenity-bdd:serenity-gradle-plugin:2.2.0")
    }
}

plugins {
    java
    jacoco
    id("io.freefair.lombok") version "5.1.0"
    id("org.springframework.boot") version "2.3.1.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
}

apply(plugin = "net.serenity-bdd.aggregator")

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-rest")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
    // Embedded mongodb for initial development
    implementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo:2.2.0")

    compileOnly("org.projectlombok:lombok:1.18.12")
    annotationProcessor("org.projectlombok:lombok:1.18.12")

    testCompileOnly("org.projectlombok:lombok:1.18.12")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.12")
    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo:2.2.0")

    // BDD tests
    testImplementation("net.serenity-bdd:serenity-core:2.2.0")
    testImplementation("net.serenity-bdd:serenity-junit:2.2.0")
    testImplementation("net.serenity-bdd:serenity-cucumber5:2.2.0")
    testImplementation("io.cucumber:cucumber-java8:5.6.0")
    testImplementation("net.serenity-bdd:serenity-screenplay:2.2.0")
    testImplementation("net.serenity-bdd:serenity-screenplay-webdriver:2.2.0")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
    if (project.hasProperty("cucumber")) {
        include("**/cucumber/**")
    } else {
        exclude("**/cucumber/**")
    }
}
tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

if (project.hasProperty("prod")) {
	tasks.withType<Jar> {
        if (!project.hasProperty("ci")) {
            dependsOn(":frontend:yarn_build")
        }

		from("frontend/build") {
			into("static")
		}
	}
}