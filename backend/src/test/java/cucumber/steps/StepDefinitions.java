package cucumber.steps;

import cucumber.screenplay.question.InvoiceItems;
import cucumber.screenplay.task.StartWith;
import cucumber.screenplay.ui.CreateInvoiceForm;
import io.cucumber.java8.En;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

public class StepDefinitions implements En {

	public StepDefinitions() {
		Given("I open {string} page", (String page) -> {
			OnStage.setTheStage(new OnlineCast());
			if ("Create Invoice".equals(page)) {
				OnStage.aNewActor().wasAbleTo(StartWith.createInvoiceForm());
			} else {
				OnStage.aNewActor().wasAbleTo(StartWith.homePage());
			}
		});
		When("I fill in {string} as {string}", (String name, String value) -> theActorInTheSpotlight().attemptsTo(Enter.theValue(value).into(CreateInvoiceForm.getField(name))));
		And("^add item$", () -> theActorInTheSpotlight().attemptsTo(Click.on(CreateInvoiceForm.ITEM_ADD_BUTTON)));
		And("save invoice", () -> {
			theActorInTheSpotlight().attemptsTo(Click.on(CreateInvoiceForm.SAVE_INVOICE));
		});
		Then("the {string} is visible as invoice item", (String title) -> theActorInTheSpotlight().should(seeThat(InvoiceItems.displayed(), hasItem(title))));
		And("amount of the {string} is {double}", (String title, Double total) -> theActorInTheSpotlight().should(seeThat(InvoiceItems.totalOf(title), is(total))));
	}
}
