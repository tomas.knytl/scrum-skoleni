package org.example.accounting.invoice;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class InvoiceTest {

//    @Test
//    void calculateTotalTwoElements() {
//        fail();
//    }
//
//    @Test
//    void calculateTotalEmpty() {
//        fail();
//    }
	@Test
	void testVatCalculation() {
		Invoice invoice = new Invoice();
		invoice.setTotal(100.0);
		invoice.calculateTotalPriceWithVat();
		Assert.assertEquals(121.00,invoice.getTotalPriceWithVat(),0);
	}
	@Test
	void testVat() {
		Invoice invoice = new Invoice();
		Assertions.assertThrows(NullPointerException.class,() -> invoice.calculateTotalPriceWithVat());
	}
}