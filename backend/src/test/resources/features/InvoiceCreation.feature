@cucumber
@invoice
Feature: Invoice Items

  As a user of Accounting app
  I want to be able to add items to invoice
  So that invoice contains items
  And total is calculated for each item

  Scenario: Create invoice
    Given I open "Create Invoice" page
    When I fill in "Quantity" as "2"
    And I fill in "Description" as "Item1"
    And I fill in "Price" as "20.0"
    And add item
    Then the "Item1" is visible as invoice item
    And amount of the "Item1" is 40.0


  Scenario: displayInvoicesWithVat
    Given I open "Create Invoice" page
    When I fill in "Quantity" as "1"
    And I fill in "Description" as "Item1"
    And I fill in "Price" as "100.0"
    And add item
    And save invoice
#    Then we are redirected to home page
#    And vatPrice of the new created Invoice is 121.00 for column VatPrice
