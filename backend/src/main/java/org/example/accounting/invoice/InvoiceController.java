package org.example.accounting.invoice;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/invoices", produces = MediaType.APPLICATION_JSON_VALUE)
public class InvoiceController {

    @Autowired
    private InvoiceRepository repository;

    @PostMapping
    ResponseEntity<Invoice> create(@RequestBody Invoice invoice) {
        System.out.println(invoice);
        invoice.setCreated(new Date());
        invoice.setTotal(invoice.calculateTotal());
        invoice.calculateTotalPriceWithVat();
        return new ResponseEntity<>(repository.save(invoice), HttpStatus.CREATED);
    }

    @GetMapping
    List<Invoice> list() {
        return repository.findAll();
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable String id) {
        repository.deleteById(id);
    }
}
