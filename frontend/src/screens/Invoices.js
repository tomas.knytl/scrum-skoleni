import React, { useState, useEffect } from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import axios from "axios";

const Invoices = () => {
  const fetchData = async () => {
    console.log("Getting invoices from remote server.");
    const result = await axios("/api/invoices/");
    console.log(result.data);
    setInvoices(result.data);
  };

  const handleDelete = (invoice_id) => {
    console.log(invoice_id);
    axios.delete("/api/invoices/" + invoice_id).then(() => {
      fetchData();
    });
  };

  const [invoices, setInvoices] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Table>
      <thead>
        <tr>
          <th>Invoice #</th>
          <th>ICO</th>
          <th>Invoice Date</th>
          <th>Due Date</th>
          <th>Total</th>
          <th>VATPrice</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {invoices.map((invoice, index) => {
          let total = invoice.items.reduce(function (prev, current) {
            return prev + current.quantity * current.unitPrice;
          }, 0);
          let dueDate = new Date(invoice.dueDate).toDateString();
          let created = new Date(invoice.created).toDateString();

          return (
            <tr key={index}>
              <td>{invoice.invoiceNumber}</td>
              <td>{invoice.ico}</td>
              <td>{created}</td>
              <td>{dueDate}</td>
              <td>{total}</td>
              <td>{invoice.totalPriceWithVat}</td>
              <td>
                <Button
                  onClick={() => {
                    handleDelete(invoice.id);
                  }}
                >
                  Delete
                </Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

//   return (
//     // {console.log(this.state.invoices)}
//     // if (!this.state.invoices.length) return null;

//     <Table>
//       <thead>
//         <tr>
//           <th>Invoice #</th>
//           <th>ICO</th>
//           <th>Invoice Date</th>
//           <th>Due Date</th>
//           <th>Total</th>
//           <th>Action</th>
//         </tr>
//       </thead>
//         //   <tbody>
//         // {invoices.map((invoice) => {
//         //   let total = invoice.items.reduce(function (prev, current) {
//         //     return prev + current.quantity * current.unitPrice;
//         //   }, 0);
//         //   let dueDate = new Date(invoice.dueDate).toDateString();
//         //   let created = new Date(invoice.created).toDateString();
//         //   let invoice_id = invoice._links.self.href.split("/api/invoices/")[1];
//         //   console.log(invoice._links.self.href);
//         //   console.log(invoice_id);
//         //   return (
//         //     <tr>
//         //       <td>{invoice.invoiceNumber}</td>
//         //       <td>{invoice.ico}</td>
//         //       <td>{created}</td>
//         //       <td>{dueDate}</td>
//         //       <td>{total}</td>
//         //       <td>
//         //         <Button
//         //           onClick={() =>
//         //             handleDelete(invoice_id, (response) => this.forceUpdate())
//         //           }
//         //         >
//         //           Delete
//         //         </Button>
//         //       </td>
//         //     </tr>
//         //   );
//         // })}
//     //   </tbody>
//     </Table>
//   );
// };

export default Invoices;
